from django.shortcuts import redirect, render
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.contrib.auth.models import User

# Create your views here.


class AccountsLoginView(LoginView):
    template_name = "registration/login.html"


class AccountsLogoutView(LogoutView):
    template_name = "registration/logged_out.html"


def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            password = request.POST.get("password2")
            user = User.objects.create_user(
                username=username, password=password
            )
            user.save()
            if user is not None:
                login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm(request.POST)
    context = {"form": form}
    return render(request, "registration/signup.html", context)
