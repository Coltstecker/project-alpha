from django.urls import path

from accounts.views import AccountsLoginView, AccountsLogoutView, signup

urlpatterns = [
    path("signup/", signup, name="signup"),
    path("login/", AccountsLoginView.as_view(), name="login"),
    path("logout/", AccountsLogoutView.as_view(), name="logout"),
]
